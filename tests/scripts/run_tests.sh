#!/usr/bin/bash

# make sure we have trace-cmd installed
if rpm -q --quiet trace-cmd; then
    :
else
    sudo dnf install -y trace-cmd
    if [[ $? != 0 ]]; then
        echo "install of trace-cmd failed!"
        exit 1
    fi
fi

# run a simple command to list available tracers
trace-cmd list || exit 2

exit 0
